import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";
import {Router, Routes} from "@angular/router";
import {OrderServices} from "../../_services/order.services";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  formData;
  constructor(private router:Router,private order:OrderServices) { }

  ngOnInit() {

  }

  beginOrder(form:NgForm){
    this.formData = form.form.value;
    let brand = this.formData['brand'];
    let season = this.formData['brand'];
    console.log(this.formData['brand']);
     this.order.brandSelected.next(brand);
     this.order.seasonSelected.next(season);
    return this.router.navigate(['escada','order']);
  }
}
