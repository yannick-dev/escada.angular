
import {RouterModule, Routes} from "@angular/router";
import {AppComponent} from "./app.component";
import {NgModule} from "@angular/core";
import {OrderComponent} from "./order/order.component";
import {HomeComponent} from "./home/home.component";
import {SellsComponent} from "./sells/sells.component";
import {ClientsComponent} from "./clients/clients.component";
import {OrderListComponent} from "./order/order-list/order-list.component";

const appRoute : Routes = [
  {path:'escada',component:AppComponent,
   children:[
     {path:'order',component:OrderComponent,
      children:[
        {path:'list',component:OrderListComponent},
      ]
     },
     {path:'home',component:HomeComponent},
     {path:'sells',component:SellsComponent},
     {path:'clients',component:ClientsComponent},
   ]},

   ];
@NgModule({
  imports:[
    RouterModule.forRoot(appRoute)
  ],
  exports:[
    RouterModule
  ]
})
export class AppRoutingModule {

}
