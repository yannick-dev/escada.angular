import { Component, OnInit } from '@angular/core';
import {OrderModel} from "../../../_models/order.model";

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent implements OnInit {
   private _listOrder : OrderModel[]=[ ];

  constructor() { }

  ngOnInit() {
  }

  get listOrder(): OrderModel[] {
    return this._listOrder;
  }
}
