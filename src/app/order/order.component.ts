import { Component, OnInit,} from '@angular/core';
import {SizeModel} from "../../_models/size.model";
import {FormArray, FormControl, FormGroup, NgForm, Validator, Validators} from "@angular/forms";
import {ItemsModel} from "../../_models/items.model";
import {OrderModel} from "../../_models/order.model";
import {OrderServices} from "../../_services/order.services";
import {of} from "rxjs/observable/of";



@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css'],
  providers:[SizeModel],
})
export class OrderComponent implements OnInit {

   brand :string='';
   season:string='';
   collection:string="PRE";
   formdata;
   formOrders :FormGroup ;
   private order:OrderModel;
  constructor(private sizes:SizeModel,private orderService:OrderServices) { }

  onSubmit(){
    this.formdata= this.formOrders.value;
    let commands = this.commandsParser(this.formdata['commands']);
    this.order = new OrderModel(12,this.brand,this.season,
                               this.collection,commands,"Created",
                               new Date().getDate().toString());
    localStorage.setItem('order-',JSON.stringify(this.order));

  }

  /*selectSize(){
    console.log("changed");
    this.sizes.setSize(this.group);
    this.size=this.sizes.size;
  }
*/
  ngOnInit() {
    this.orderService.seasonSelected.subscribe(
      (season:string)=>{
        this.season =season;
      }
    );
    this.orderService.brandSelected.subscribe(
      (brand:string)=>{
        this.brand =brand;
      }
    );
    this.formOrders =new FormGroup({
      'commands': new FormArray([this.form])
    });
  }

  addCommandLine(){

    (<FormArray>this.formOrders.get('commands')).push(this.form);
  }
  getControls(){
   return (<FormArray> this.formOrders.get('commands')).controls;
  }
  saveAsExcel(){
      var js = localStorage.getItem('order-');
      var r= this.JsontoExeclParser(js);
      console.log(r);
     this.orderService.exportAsExcelFile(r,'confirmation_list')
  }

  private commandsParser(commands:object[]):ItemsModel[]{
    let group;let name;let vendor;
    let price; let quantity; let size;
    let description;let color; let codeClr;
    let result:ItemsModel[]=[];
    for (var i=0;i<commands.length;i++){
      let command = commands[i];
      group = command['group'];
      quantity = command['quantity'];
      name = command['name'];
      vendor = command['vendor'];
      price = command['price'];
      size = command['size'];
      color = command['color'];
      codeClr = command['code_color'];
      description = command['description'];

      result.push(new ItemsModel(group,vendor,name,codeClr,color,
        size,quantity,price,description) );
    }

     return result;
  }

  private JsontoExeclParser(json:string):any[]{
    var result:any[]=[];
    let js = JSON.parse(json);
    let items = js['_items'];
     for(var i=0;i<items.length;i++){
       var item = items[i];
       var newJson = {
         "Бренд":this.brand,
         "Сезон":this.season,
         "Группа":item['_group'],
         "Артикул":item['_vendor'],
         "Наиминование":item['_name'],
         "Код Цвета":item['_code_color'],
         "Размер":item['_code_color'],
         "Цвет":item['_color'],
         "Цена":item['_price'],
         "Количество":item['_quantity'],
         "Описание":item['_description'],

       };
       result.push(newJson);
     }
    return result;
  }

  form = new FormGroup({
    'group':new FormControl('Pullovers',[Validators.required]),
    'vendor':new FormControl(null,[Validators.required]),
    'name':new FormControl('блузка'),
    'code_color':new FormControl(null),
    'color':new FormControl(null),
    'description':new FormControl(null),
    'composition':new FormControl(null),
    'size':new FormControl(null,[Validators.required]),
    'price':new FormControl(null,[Validators.required,Validators.pattern('^[1-9]+[0-9]*$')]),
    'quantity':new FormControl(null,[Validators.required,Validators.pattern('^[1-9]+[0-9]*$')]),

  });


}
