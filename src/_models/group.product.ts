
import {SizeModel} from "./size.model";

export class GroupProduct {
  private static names = {
    'jeans':{'en':'5-pocket Hose', 'ru':'джинсы'},
    'bluse':{'en':'5-pocket Hose', 'ru':'блузка'},
    'foulard':{'en':'5-pocket Hose', 'ru':'платок'},
    'hose':{'en':'5-pocket Hose', 'ru':'брюки'},
    'jacke':{'en':'5-pocket Hose', 'ru':'жакет'},
    'jumpsuit':{'en':'5-pocket Hose', 'ru':'комбинезон'},
    'kleid':{'en':'5-pocket Hose', 'ru':'платье'},
    'outerwear_jacke':{'en':'5-pocket Hose', 'ru':'куртка'},
    'outerwear_mantel':{'en':'5-pocket Hose', 'ru':'пальто'},
    'pullover':{'en':'5-pocket Hose', 'ru':'джемпер'},
    'rock':{'en':'5-pocket Hose', 'ru':'юбка'},
    'stirckjacke':{'en':'Strickjacke', 'ru':' кардиган'},
    'sweatshirt':{'en':'Sweatshirt', 'ru':'джемпер'},
    'top':{'en':'Top', 'ru':'топ'},
    't-shirt':{'en':'T-Shirt', 'ru':'футболка'},
  };
  private static groups = {
    'accessories':{'en':'Accessories', 'ru':'аксессуары'},
    'accessor':{'en':'5-pocket Hose', 'ru':'платье'},
    'acces':{'en':'5-pocket Hose', 'ru':'низ'},
    'accessorie':{'en':'5-pocket Hose', 'ru':'верхняя одежда'},
    'accessors':{'en':'5-pocket Hose', 'ru':'низ'},
    'accessies':{'en':'5-pocket Hose', 'ru':'аксессуары'},
    'acceories':{'en':'5-pocket Hose', 'ru':'аксессуары'},
  };


 private group:string;
 private vendor_group:string;
 private name:string[];
 private size:SizeModel;


}
