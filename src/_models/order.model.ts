import {ItemsModel} from './items.model';

export class OrderModel {


private _numOrder: number;
private _brand:string;
private _season:string;
private _collection:string;
private _items: ItemsModel[];
private _status: string;
private _createdAt:string;

  constructor(numOrder: number, brand: string,
              season: string, collection: string,
              items: ItemsModel[], status: string, createdAt: string) {
    this._numOrder = numOrder;
    this._brand = brand;
    this._season = season;
    this._collection = collection;
    this._items = items;
    this._status = status;
    this._createdAt = createdAt;
  }

  get createdAt(): string {
    return this._createdAt;
  }

  get numOrder(): number {
    return this._numOrder;
  }

  get items(): ItemsModel[] {
    return this._items;
  }

  get status(): string {
    return this._status;
  }

  get brand(): string {
    return this._brand;
  }

  get season(): string {
    return this._season;
  }

  get collection(): string {
    return this._collection;
  }
}
